package be.sanxys.configannotation.tests;

public class User{

	private String name;
	private int age;
	private Sexe sexe;
	
	public User(String name, int age, Sexe sexe) {
		this.name = name;
		this.age = age;
		this.sexe = sexe;
	}
	
	public enum Sexe {
		MALE;
	}
	
}
