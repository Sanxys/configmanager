package be.sanxys.configannotation.tests;

import be.sanxys.configannotation.ConfigInitier;
import be.sanxys.configannotation.Side;

public class Test {

	private static ConfigInitier configInitier = new ConfigInitier();
	
	public static void main(String[] args) {
		configInitier.init(Configs.class);
		configInitier.saveAll();
	}
	
}
